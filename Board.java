public class Board{
	private Square[][] tictactoeboard;
	
	public Board(){
		tictactoeboard=new Square[3][3];
		for(int i = 0;i < tictactoeboard.length;i++){
			for(int j = 0;j < tictactoeboard[i].length;j++){
				tictactoeboard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		//num variable is used to display row number
		int num = 1;
		String msg = "  1 2 3\n";
		for(int i = 0; i < tictactoeboard.length;i++){
			msg += num + " ";
			for(int j = 0; j < tictactoeboard[i].length;j++){
				msg += tictactoeboard[i][j] + " ";
			}
			msg+="\n";
			num++;
		}
		return msg;
	}
	
	public boolean placeToken(int row,int col,Square playerToken){
		//validate the row & column for 3x3 board and no input <= 0
		if( (row-1) >= 3 || (col-1) >= 3 || row <= 0 || col <= 0){
			return false;
		}
		
		if (tictactoeboard[row-1][col-1] == Square.BLANK){
			tictactoeboard[row-1][col-1] = playerToken;
			return true;
		}else{
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for(int i = 0;i < tictactoeboard.length;i++){
			for(int j = 0; j < tictactoeboard[i].length;j++){
				if(tictactoeboard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0;i < tictactoeboard.length;i++){
			int count = 0;
			for(int j = 0;j < tictactoeboard[i].length;j++){
				if(tictactoeboard[i][j] == playerToken){
					count++;
				}
			}
			if(count == 3){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){	
		for(int i = 0;i < tictactoeboard.length;i++){
			int count = 0;
			for(int j = 0;j < tictactoeboard[i].length;j++){
				if(tictactoeboard[j][i] == playerToken){
					count++;
				}
			}
			if(count == 3){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean checkIfWinningDiagonal(Square playerToken){
		//bonus
		if(tictactoeboard[1][1] != playerToken){
			return false;
		}
		if(tictactoeboard[0][0] == playerToken && tictactoeboard[2][2] == playerToken){
			return true;
		}else if(tictactoeboard[0][2] == playerToken && tictactoeboard[2][0] == playerToken){
			return true;
		}else{
			return false;
		}
	}
}