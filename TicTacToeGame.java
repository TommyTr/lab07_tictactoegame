import java.util.Scanner;
public class TicTacToeGame{
	
	public static void main(String[] args){
		
		System.out.println("Welcome to the Tic Tac Toe game!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
		Board board=new Board();
		boolean gameOver=false;
		int player=1;
		Square playerToken=Square.X;
		Scanner sc=new Scanner(System.in);
		
		while(gameOver==false){
			System.out.println(board);
			if(player==1){
				playerToken=Square.X;
			}else{
				playerToken=Square.O;
			}
			boolean isValid=false;
			
			while(isValid==false){
				System.out.println("Player "+player+": It's your turn. Where do you want to place your token? Enter a valid row and then a valid column");
				int row=sc.nextInt();
				int col=sc.nextInt();
				isValid=board.placeToken(row,col,playerToken);
			}
			
			if(board.checkIfWinning(playerToken)){
				System.out.println(board);
				System.out.println("Player "+player+" won!");
				gameOver=true;
			}else if(board.checkIfFull()){
				System.out.println(board);
				System.out.println("It is a tie!");
				gameOver=true;
			}else{
				player++;
				if(player>2){
					player=1;
				}
			}
		}
	}
}